<?php
include 'koneksi.php';

session_start();
if (!(isset($_SESSION['login']))) {
    header("location:home.php");
    exit;
}

if (isset($_POST['save'])){
$username = $_POST['username'];
$phone = $_POST['phone'];
$passwd1 = $_POST['passwd1'];
$passwd2 = $_POST['passwd2'];
$id = $_SESSION['id'];

if ($passwd1 == $passwd2) {
    $user = mysqli_query($koneksi, "UPDATE users_table SET username = '$username', mobile_number = '$phone', passwd = '$passwd1' WHERE id = '$id';");
    if ($user) {
        echo "<script>alert('Success Update'); window.location.href='profile.php'</script>";
        $_SESSION['username'] = $username;
    }else{
        echo "<script>alert('Gagal Update'); window.location.href='profile.php'</script>";
    }
}else{
    echo "<script>alert('Password tidak sama'); window.location.href='profile.php'</script>";
}
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Profile</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
    
<nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href=""> <img src="EAD.png" alt="logo" width=100px> </a> 
    </div>
        <ul class="nav navbar-nav navbar-right">
            <li><a class = "fas fa-shopping-cart" href="cart.php"></a></li>
            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"> <?php echo $_SESSION['username']?> <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="profile.php">Update Profile</a></li>
                <li><a href="logout.php">Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>

    <form action="profile.php" method="POST">
    
    <div style = "width:50%; margin:auto;">
    <div class="card-deck">
    <div class="card">
    <div class="card-body">
    <h5 class="card-title">Register</h5>
    
    <label for="username"></label>Email
    <label for="mail"><?php echo $_SESSION['mail']?></label><br>
    <label for="username"></label>Username
    <br>
    <input type="text" name="username" id="" placeholder="Username" style = "width:100%;" required ><br>
    <label for="mobilenumber"></label>Mobile Number
    <br>
    <input type="number" name="phone" id="" placeholder="Mobile Number" style = "width:100%;" required><br>

    <hr>
    
    <label for="newpasswd"></label>New Password
    <br>
    <input type="passwd" name="passwd1" id="" placeholder="New Password" style = "width:100%;" required><br>
    <label for="passwd"></label>Confirm Password
    <br>
    <input type="passwd" name="passwd2" id="" placeholder="Confirm Password" style = "width:100%;" required><br>

    <br>

    <input type="submit" name="save" value="Save" style = "width:100%;">
    <input type="submit" name="close" value="Cancel" onclick="window.location.href = 'homelog.php';" style = "width:100%;">
    
    </form>
</body>
</html>
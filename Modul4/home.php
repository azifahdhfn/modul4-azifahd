<?php
session_start();
if ((isset($_SESSION['login']))) {
    header("location:homelog.php");
    exit;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Home</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>

<nav class="navbar navbar-default navbar-static-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="home.php"> <img src="EAD.png" alt="logo" width=100px> </a> 
    </div>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="login.php">Login</a> 
      <a href="register.php">Register</a></li>
    </ul>
  </div>
</nav>

<hr>
<br>

<div style = "margin:auto;">
<div class="container">
<div class="jumbotron jumbotron-fluid" style = "background-color:#7ed6df;">
    <div class="container">
    <h1 class="display-4">Hello Coders</h1>
    <p class="lead">Welcome to our store, please take a look for the product you might buy.</p>
  </div>
</div>
</div>

<div style = "width:82%; margin:auto;">
<div class="card-deck">
    <div class="card">
      <img src="browser.png" class="card-img-top" alt="browser">
      <div class="card-body">
        <h5 class="card-title">Learning Basic Web Programming</h5>
        <h6>Rp.210.000,-</h6>
        <p class="card-text">Want to be able to make a website? Learn basic components such as HTML, CSS, and JavaScript in this class curriculum.</p>
        <hr>
        <input type="button" value="buy" onclick="window.location.href = 'login.php';" style = "width:100%; padding:2%;">
      </div>
  </div>
  <div class="card">
      <img src="java.png" class="card-img-top" alt="java">
      <div class="card-body">
        <h5 class="card-title">Starting Programming in Java</h5>
        <h6>Rp.150.000,-</h6>
        <p class="card-text">Learn Java Language for you who want to learn the most popular Object-Oriented Programming (PBO) concepts for developing applications.</p>
        <hr>
        <input type="button" value="buy" onclick="window.location.href = 'login.php';" style = "width:100%; padding:2%;">
      </div>
  </div>
  <div class="card">
      <img src="python.png" class="card-img-top" alt="python">
      <div class="card-body">
        <h5 class="card-title">Starting Programming in Phyton</h5>
        <h6>Rp.200.000,-</h6>
        <p class="card-text">Learn Python - Fundamental various current industry trends: Data Science, Machine Learning Infrastructure-management.</p>
        <hr>
        <input type="button" value="buy" onclick="window.location.href = 'login.php';" style = "width:100%; padding:2%;";>
      </div>
  </div>
</div>
</div>
</body>
</html>
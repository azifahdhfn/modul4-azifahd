<?php
include "koneksi.php";
session_start();
$id = $_SESSION['id'];

if (!(isset($_SESSION['login']))) {
    header("location:home.php");
    exit;
}

if (isset($_POST['buy1']) || isset($_POST['buy2']) || isset($_POST['buy3']) ){
    if (isset($_POST['buy1'])){
        $harga = "210000";
        $product = "browser"; 
    }else if (isset($_POST['buy2'])){
        $harga = "150000";
        $product = "java"; 
    }else{
        $harga = "200000";
        $product = "python"; 
    }
    $query = mysqli_query($koneksi,"INSERT INTO cart_table (id, user_id, product, price) VALUES (NULL, '$id', '$product', '$harga');");
    if ($query) {
        echo "<script>alert('Berhasil Dimasukkan ke Cart'); window.location.href='cart.php'</script>";
    }else{
        echo "<script>alert('Gagal Dimasukkan ke Cart'); window.location.href='cart.php'</script>";
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Cart</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>

<nav class="navbar navbar-default navbar-static-top">
  <div class="container-fluid">
  <div class="navbar-header">
      <a class="navbar-brand" href="homelog.php"> <img src="EAD.png" alt="logo" width=100px> </a> 
    </div>
    <ul class="nav navbar-nav navbar-right">
        <li><a class = "fas fa-shopping-cart" href="cart.php"></a></li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"> <?php echo $_SESSION['username']?> <span class="caret"></span></a>
        <ul class="dropdown-menu">
            <li><a href="profile.php">Update Profile</a></li>
            <li><a href="logout.php">Logout</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>

<hr>
<br>
  
<form action="checklog.php" method="POST">
<div style = "width:50%; margin:auto;">
<div class="card-deck">
    <div class="card">
        <div class="card-body">
        <td><h5 class="card-title">Cart</h5></td>
        <table>
            <tr>
                <th >No</th>
                <th>Product</th>
                <th >Price</th>
                <th>action</th>
            </tr>

            <?php 
            $no = 1;
            $total = 0;
			    $query = mysqli_query($koneksi,"SELECT * FROM cart_table WHERE user_id = '$id'") or die ("Query salah");
			    foreach ($query as $data) { 		
				    ?>
				    <tr>
					    <td><?= $no++; ?></td>
					    <td><?= $data['product'] ?></td>
                        <td><?= $data['price'] ?></td>
                   
					    <td><a href="delete.php?id=<?=$data['id']?>">Hapus</a>
					    </td>
				    </tr>
                    <?php 
                    $total = $total + $data['price'];
			    }
			    ?>
        </table>
        <hr>
        Total <?php echo $total ?>
        </div>
    </div>
</form>
</body>
</html>